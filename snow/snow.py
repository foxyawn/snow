#!/usr/bin/env ipython

import curses
from time import sleep
from random import randint,random

def drawf(scr,f,flop=False):
	def char(f,x):
		if x<=0: return char(f,x+1)
		if x>=len(f): return char(f,x-1)
		
		a,b,c=[int(i) for i in f[x-1:x+2]]

		if a<b<c: return '\\'
		if a>b>c: return '/'
		if f[x]-b > 2/3: return '_'
		if f[x]-b < 1/3: return '"'
		return '-'
	#============
	if flop:
		h,_=scr.getmaxyx()
		g=[h-y for y in f]
		return drawf(scr,g)

	for x,y in enumerate(f):
		z=int(y)
		try:
			scr.addstr(z,x,char(f,x))
		except:
			scr.addstr(0,0,f"Error at: x:{x} y:{y:.3f}")
#=============================================================================
class Flake():
	'''a single snowflake
	'''
	def __init__(self,scr,coord,ground):
		self.scr=scr
		self.coord=coord
		self.ground=ground
	def __call__(self):
		self.coord[0]+=1				# move down
		if self.coord in self.ground:	# if colision with ground
			self.ground.add(self.coord)	# add to ground
			# self.ground()				# redraw ground
			X=self.scr.getmaxyx()[1]
			self.coord=[0,randint(0,X-1)]				# put back at top, at a random x value
		self.scr.addstr(*self.coord,'*')# draw
#=============================================================================
class Ground():
	def __init__(self,scr,level,deviation):
		self.scr=scr
		self.generate(level,deviation)
	def generate(self,level,deviation):
		'''will never draw to lower right hand side to avoid errors
		meaning that level=0 -> layer 1
		'''
		# X=self.scr.getmaxyx()[1]-1
		self.stack=[abs(level)+2]*self.scr.getmaxyx()[1]
	def __call__(self):
		'''will still break if window size is too small -_- but 6 lines it a bit ridiculious, idk.'''
		if self.max > self.scr.getmaxyx()[0]*0.2: # taking up 20% of screen
			self.melt()
		# y,_=self.scr.getmaxyx()
		# if random()<0.1:self.smooth()
		self.smooth()
		drawf(self.scr,self.stack,flop=True)
		# for x,s in enumerate(self.stack):
		# 	self.scr.addstr(y-s,x,'-')
	def melt(self):
		'''removes a layer from bottom of stack
		melt with smoothing function? sounds smart :)
		melt with perlin noise later? that sounds fun :b
		melt by removing snow in a fifo system? literally store flakes!?!
		'''
		self.stack=[s-1/9 if s>2 else 2 for s in self.stack]
		# self.smooth
	def smooth(self):
		f=self.stack
		f=[f[0]]+f+[f[-1]]
		weight=[0.01,0.98,0.01]
		mult=lambda A,B: [a*b for a,b in zip(A,B)]	# mult two vectors component my component
		g=[sum(mult(f[x:x+3],weight)) for x in range(len(f)-2)]	# distribute snow
		self.stack=[y if y>2 else 2 for y in g]	# make sure all stay above 2
	def __contains__(self,coord):
		return self.stack[coord[1]] >= self.scr.getmaxyx()[0]-coord[0]
	def add(self,coord):
		# self.stack[coord[1]]+=1/3
		# lets try splatting
		x=coord[1]
		self.stack[x]+=1/2
		if x>0: self.stack[x-1]+=1/4
		if x<len(self.stack)-1: self.stack[x+1]+=1/4
	@property
	def min(self): return min(self.stack)
	@property
	def max(self): return max(self.stack)
#=============================================================================
def scene(stdscr):
	# s=0
	curses.halfdelay(1)
	Y,X=stdscr.getmaxyx()
	density=0.02
	ground=Ground(stdscr,0,0)
	ground()
	# use perlin noise to generate flakes, such that ground stays relatively flat.
	# use perlin vector map to direct flakes?
	# have perlin gusts of win to move fallen snow?
	# poisson-distributed flakes would look neat too

	storm=[Flake(stdscr,[randint(0,Y-3),randint(0,X-1)],ground) for x in range(int(X*Y*density))]
	while True:
		stdscr.clear()		# clear screen
		for f in storm: f()	# move and draw each flake
		ground()
		# stdscr.refresh()
		if stdscr.getch()>0: break
		# sleep(.1)
		# s+=1
#=============================================================================
def main():
	curses.wrapper(scene)

if __name__=="__main__":
	main()